#%% imports
import pandas as pd
import matplotlib.pyplot as plt
from path import Path
import inspect
import numpy as np
from pprint import pprint
import copy
from tqdm import tqdm
import seaborn as sns

WORKING_DIR = Path(inspect.getsourcefile(lambda: 0)).abspath().parent
DATA_DIR = WORKING_DIR / 'data'

n_schools = 8
data = pd.DataFrame(columns=['weekly_hours', 'school_id'])

#%% reading the data
for id in range(1, n_schools + 1):
    school = pd.read_csv(DATA_DIR / f'./school{id}.dat', names=['weekly_hours'])
    school['school_id'] = id
    data = data.append(school)
data = data.reset_index(drop=True)
pd.concat((data.head(), data.tail()))
#%% initializing parameters and defining hyperparameters
n_iterations = 4000
n_examples = data.shape[0]
mu0 = 7
gamma20 = 5
tau20 = 10
eta0 = 2
s20 = 15
nu0 = 2
m = n_schools
num_stud_per_group = data['school_id'].value_counts().tolist()
sample_averages_per_group = data.groupby('school_id')['weekly_hours'].mean()\
                                                                     .tolist()
sample_average = data['weekly_hours'].mean()

mu = mu0
mu_buffer = [mu0]
tau2 = tau20
tau2_buffer = [tau20]
thetas_per_group = sample_averages_per_group
thetas_per_group_buffer = [copy.deepcopy(sample_averages_per_group)]
s2 = s20
s2_buffer = [s20]
print(sample_average)
np.random.seed(42)
#%% gibbs sampling
for i in tqdm(range(n_iterations), ncols=100):
    theta_average = np.sum(
        [x * w for x, w in zip(thetas_per_group, num_stud_per_group)]
    ) / n_examples

    # mu \sim normal(b/a, 1/a)
    a_tmp = (m/tau2 + 1/gamma20)
    b_tmp = (m * theta_average)/tau2 + mu0/gamma20
    mu = np.random.normal( b_tmp/a_tmp, np.sqrt(1 / a_tmp) )

    # tau2
    tau2_second_parameter = 1 / (
        (
        eta0 * tau20 +
        sum( map(lambda x: (x - mu)**2, thetas_per_group) )
        ) / 2
    )
    tau2 = 1 / np.random.gamma( (eta0 + m) / 2, tau2_second_parameter )

    # all_groups_theta
    for j in range(n_schools):
        a_tmp = num_stud_per_group[j] / s2 + 1 / tau2
        b_tmp = (num_stud_per_group[j] * theta_average) / s2 + mu / tau2
        thetas_per_group[j] = np.random.normal(
            b_tmp / a_tmp,
            1 / np.sqrt(a_tmp)
        )

    # s2
    custom_var = 0
    for idx in range(n_examples):
        custom_var += ((
            data['weekly_hours'][idx] -
            thetas_per_group[data['school_id'][idx] - 1]
        )**2)
    s2_second_parameter = 1 / (
            ( nu0 * s20 + custom_var ) / 2
    )
    s2 = 1 / np.random.gamma( (nu0 + n_examples) / 2, s2_second_parameter )
    mu_buffer.append(mu)
    tau2_buffer.append(tau2)
    thetas_per_group_buffer.append(copy.deepcopy(thetas_per_group))
    s2_buffer.append(s2)

# %% trace plots of mu, tau2 and s2
fig, ax = plt.subplots(nrows=3, ncols=1, figsize=(15, 15))
ax[0].plot(mu_buffer)
ax[0].set_ylabel('mu', fontsize=14)
ax[0].hlines(np.mean(mu_buffer), xmin=0, xmax=n_iterations, color='red')
ax[1].plot(tau2_buffer)
ax[1].set_ylabel('tau2', fontsize=14)
ax[1].hlines(np.mean(tau2_buffer), xmin=0, xmax=n_iterations, color='red')
ax[2].plot(s2_buffer)
ax[2].set_ylabel('s2', fontsize=14)
ax[2].hlines(np.mean(s2_buffer), xmin=0, xmax=n_iterations, color='red')
plt.savefig('../figures/mu_tau2_s2_trace_plots.png')
plt.show()
#%% trace plots of all the thetas
fig, ax = plt.subplots( nrows=n_schools, ncols=1, figsize=(15, 20) )
fig.suptitle('Trace plots of all schools theta', fontsize=16)
for j in range(n_schools):
    ax[j].plot([thetas[j] for thetas in thetas_per_group_buffer])
    ax[j].set_ylabel('theta_' + str(j + 1), fontsize=14)
plt.savefig('../figures/all_groups_theta_trace_plots.png')
plt.show()
#%% autocorrelation function until lag 20 of mu, tau2 and s2
def autocorrelation(x, t):
    'computes acf at lag t of the sequence x'
    return np.corrcoef(x[:len(x)-t], x[t:])[1, 0]

acf_mu = [autocorrelation(mu_buffer, t) for t in range(20)]
acf_tau2 = [autocorrelation(tau2_buffer, t) for t in range(20)]
acf_s2 = [autocorrelation(s2_buffer, t) for t in range(20)]
fig, ax = plt.subplots(nrows=3, ncols=1, figsize=(15, 15))
for x, y in enumerate(acf_mu):
    ax[0].scatter(x, y, c='black')
    ax[0].vlines(x=x, ymin=0, ymax=y)
    ax[0].set_title('mu')
for x, y in enumerate(acf_tau2):
    ax[1].scatter(x, y, c='black')
    ax[1].vlines(x=x, ymin=0, ymax=y)
    ax[1].set_title('tau2')
for x, y in enumerate(acf_s2):
    ax[2].scatter(x, y, c='black')
    ax[2].vlines(x=x, ymin=0, ymax=y)
    ax[2].set_title('s2')
plt.savefig('../figures/autocorrelations_until_lag_20.png')

def effSize(x):
    """
    Computes the effective size of an MCMC.
    Source:
    https://mc-stan.org/docs/2_20/reference-manual/\
effective-sample-size-section.html
    """
    return (int(np.ceil(
        len(x) / (1 + 2 * np.sum([autocorrelation(x, t) for t in [1, 2, 3]]))
    )))

print('effective sample size of mu: ', effSize(mu_buffer))
print('effective sample size of tau2: ', effSize(tau2_buffer))
print('effective sample size of s2: ', effSize(s2_buffer))
print(
    'minimum number of iterations needed for an effective sample size of 1000:',
    int(np.ceil(n_iterations/487 * n_iterations))
)
#%% (c)
# the between-school variation doesn't catch much of the total variance
plt.figure(figsize=(15, 6))
R2 = [ t / (t + s) for t, s in zip(tau2_buffer, s2_buffer)]
sns.distplot(R2, axlabel=r'$\frac{\tau^2}{\tau^2 + \sigma^2}$')
plt.savefig('../figures/dist_plot_R2.png')
plt.show()
#%% (d)
prob_7_less_6 = np.mean(
    [a < b for a, b in zip(
    [x[6] for x in thetas_per_group_buffer],
    [x[5] for x in thetas_per_group_buffer])]
)
print('probability theta_7 < theta_6:\t', prob_7_less_6)
prob_7_less_all = [True] * n_iterations
for i in range(n_iterations):
    for j in [0, 1, 2, 3, 4, 5, 7]:
        if thetas_per_group_buffer[i][6] > thetas_per_group_buffer[i][j]:
            prob_7_less_all[i] = False
print('probability theta_7 < all other theta:\t', np.mean(prob_7_less_all))
#%% (e)
plt.figure(figsize=(12, 6))
plt.scatter(sample_averages_per_group, np.mean(thetas_per_group_buffer, 0))
x = np.arange(5, 9, 1e-2)
y = x
plt.plot(x, y)
plt.savefig('../figures/real_vs_predicted.png')

print('sample mean: {0:4f}'.format(sample_average))
average_thetas_per_group = np.sum(
    [x * w for x, w in zip(
        np.mean(thetas_per_group_buffer, 0), num_stud_per_group)]
) / n_examples
print('mean of all extraceted theta: {0:4f}'.format(average_thetas_per_group))

# %%
