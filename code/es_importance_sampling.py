#%% importing libraries and defining some variables
import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import beta

np.random.seed(42)

n = 10000
w1 = 0.4
w2 = 0.7

supp = np.random.uniform(0, 1, n)
supp = np.sort(supp)
beta1 = beta.pdf(supp, 5, 2)
beta2 = beta.pdf(supp, 2, 8)
mixture = beta1 * w1 + beta2 * w2

#%% plotting the pdf of the mixture
plt.plot(supp, mixture)
plt.title("MIXTURE of 0.3 * B(5,2) + 0.7 * B(2, 8)")
plt.show()

#%% computing expected value and the prob of the mixture in [0.45 - 0.55] 
print("Expected value mixture: ", np.matmul(mixture, supp)/n)
crit1 = supp >= 0.45
crit2 = supp <= 0.55
crit = crit1 == crit2
print("Prob on [0.45 - 0.55]: ", sum(mixture[crit]) / n)
