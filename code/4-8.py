#%%
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from scipy.stats import gamma
from scipy.stats import poisson
from collections import Counter
import math

np.random.seed(42)

def read_dat(filename):
    symbols = ['\n', ' ']
    data = []
    with open(filename) as f:
        data += f.read()
    return [int(x) for x in data if x not in symbols]

#%%
bach_filename = "./data/menchild30bach.dat"
nobach_filename = "./data/menchild30nobach.dat"
bach = np.sort(read_dat(bach_filename))
nobach = np.sort(read_dat(nobach_filename))

print("bach data:\n", bach)
print("no bach data:\n", nobach)

#%% (a)
n_samples = 5000
bach_a = np.sum(bach) + 2
bach_b = len(bach) + 1
nobach_a = np.sum(nobach) + 2
nobach_b = len(nobach) + 1
inv_bach_b = 1/bach_b
inv_nobach_b = 1/nobach_b
print("bach_a: ", bach_a)
print("bach_b: ", bach_b, "inv_bach_b: ", inv_bach_b)
print("nobach_a: ", nobach_a)
print("nobach_b: ", nobach_b, "inv_nobach_b: ", inv_nobach_b)

post_bach = np.random.gamma(bach_a, inv_bach_b, n_samples)
post_nobach = np.random.gamma(nobach_a, inv_nobach_b, n_samples)
plt.figure(figsize=(12, 8))
plt.grid(True, alpha=0.3)
plt.hist(post_bach, alpha=0.5)
plt.hist(post_nobach, alpha=0.5)
diff_theta = post_nobach - post_bach
plt.hist(diff_theta, alpha=0.5)
plt.legend(['post - bach', 'post - nobach', 'diff_theta (nobach - bach)'])
plt.show()

#%%
post_pred_bach = pd.Series([np.random.poisson(theta) for theta in post_bach])
post_pred_nobach = pd.Series([np.random.poisson(theta) for theta in post_nobach])
diff = pd.Series([x - y for x, y in zip(post_pred_nobach, post_pred_bach)])

# %%
plt.figure(figsize=(12, 8))
plt.title('Posterior predictive distribution nobach and bach', fontsize=14)
plt.xlabel('# children', fontsize=14)
plt.grid(True, alpha=0.3)
plt.bar(post_pred_bach.value_counts().index, post_pred_bach.value_counts(), alpha=0.5)
plt.bar(post_pred_nobach.value_counts().index, post_pred_nobach.value_counts(), alpha=0.5)
plt.legend(['post_pred - bach', 'post_pred - nobach'])
plt.show()

#%%
plt.figure(figsize=(12, 8))
plt.title('Posterior predictive distribution diff(nobach - bach)', fontsize=14)
plt.grid(True, alpha=0.3)
plt.bar(diff.value_counts().index, diff.value_counts(), alpha=0.5)
plt.show()

# %% (b)
diff_theta_sorted = np.sort(diff_theta)
left_ci_idx = int(np.ceil(n_samples * 0.025))
right_ci_idx = int(np.ceil(n_samples * 0.975))
diff_theta_left_ci  = diff_theta_sorted[left_ci_idx]
diff_theta_right_ci = diff_theta_sorted[right_ci_idx]
print('Confidence interval of the predictive posterior distribution of the thetas:')
print('( ', diff_theta_left_ci, ', ', diff_theta_right_ci, ' )', sep='')

#%%
diff_sorted = np.sort(diff)
left_ci_idx = int(np.ceil(n_samples * 0.025))
right_ci_idx = int(np.ceil(n_samples * 0.975))
diff_left_ci  = diff_sorted[left_ci_idx]
diff_right_ci = diff_sorted[right_ci_idx]
print('Confidence interval of the predictive posterior distribution of the thetas:')
print('( ', diff_left_ci, ', ', diff_right_ci, ' )', sep='')

# %% (c)
nobach_series = pd.Series(nobach)
sampled_poiss = pd.Series(np.random.poisson(1.4, n_samples))
plt.figure(figsize=(12, 8))
plt.title('Comparing distributions of menchild30nobach', fontsize=14)
plt.grid(True, alpha=0.3)
plt.bar(
    nobach_series.value_counts().index,
    nobach_series.value_counts() / nobach_series.value_counts().sum(),
    alpha=0.5
)
plt.bar(
    sampled_poiss.value_counts().index,
    sampled_poiss.value_counts() / sampled_poiss.value_counts().sum(),
    alpha=0.5
)
plt.legend(['empirical distribution', 'poisson (theta=1.4)'])
plt.show()

#%%
freq_counter = 0
for obs in nobach:
    for test in sampled_poiss:
        if (test >= obs):
            freq_counter += 1
print(freq_counter / (len(nobach)*n_samples))

# %% (d)
n_B = 218
ones = []
zeros = []
all_samples = [np.random.poisson(theta, n_B) for theta in post_nobach]
for sample in all_samples:
    ones_counter = 0
    zeros_counter = 0
    for i in range(len(sample)):
        if (sample[i] == 1):
            ones_counter += 1
        if (sample[i] == 0):
            zeros_counter += 1
    ones.append(ones_counter)
    zeros.append(zeros_counter)

# %%
nobach_counter = Counter(nobach)

plt.figure(figsize=(12, 8))
plt.title('zeros vs ones', fontsize=14)
plt.grid(True, alpha=0.3)
plt.scatter(zeros, ones)
plt.scatter(nobach_counter[0], nobach_counter[1], s=200, c='blue')
plt.show()
