import math
import numpy as np
from itertools import product
import matplotlib.pyplot as plt

def dgalenshore(y, a, b):
    return (
        (2 / math.gamma(a))
        * b ** (2 * a)
        * y ** (2 * a + 1)
        * math.exp(-b ** 2 * y ** 2)
    )

all_a = np.linspace(3, 5, 2)
all_b = np.linspace(3, 5, 2)
all_x = np.linspace(0, 1.75, 1000)
all_values = list(product(all_a, all_b))

plt.figure(figsize=(20, 10))
plt.grid(True)
for (a, b) in all_values:
    plt.plot(all_x, list(map(lambda x: dgalenshore(x, a, b), all_x)))
plt.legend([f'a={a}, b={b}' for a, b in all_values], shadow=1)
plt.show()
#plt.savefig('../figures/galenshore.png')
